# DoveMAC

Implementations of DoveMAC and ZMAC1 with SKINNY-64-128 for the arduino
platform. 

### Implementor

Tony Grochow

### Code

The plain implementations of DoveMAC and ZMAC1 can be found in 
`arduino/libraries/mac`

### Environment

Besides a microcontroller and connections, the arduino environment and setup are needed

- `sudo apt-get arduino` if available
- The Arduino IDE: 
  
  `https://www.arduino.cc/en/Main/Software`

Compilation and upload to the microcontroller can be done from the Arduino IDE

### Dependencies

The code comes with an implemenation of SKINNY-64-128 from rweather:

`https://github.com/rweather/skinny-c`

Licensed under MIT, available in this repository and the corresponding software
parts.

### Tested

Tested with 2018-11 Archlinux with 4.19 kernel with ATmega 328p and
ATmega 2560.
