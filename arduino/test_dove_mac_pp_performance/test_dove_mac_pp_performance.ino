#include "dove_mac_pp.h"
#include "predefined_messages/message_64B.h"

// test results for the Atmega2560:
// 64 byte
// 46.33us per byte, 21582.60 bytes per second
// tag := 679B57AB24B7F530

// 128 byte
// 37.40us per byte, 26735.94 bytes per second
// tag := C6E906AABB009DA0

// 256 byte
// 32.94us per byte, 30360.52 bytes per second
// tag := 823C58872321394F

// 512 byte
// 30.70us per byte, 32568.15 bytes per second
// tag := 6EDAC26C4755A6B2

// 1024 byte
// 29.59us per byte, 33796.91 bytes per second
// tag := 3477763CA9DA92FE

// 2048 byte
// 29.03us per byte, 34446.74 bytes per second
// tag := B1F33B1F2E3A3D5A

// 4096 byte
// 28.75us per byte, 34781.10 bytes per second
// tag := A2DAC152E4221901

// test results for the Atmega328p:
// 64 byte
// 46.22us per byte, 21636.21 bytes per second
// tag := 679B57AB24B7F530

// 128 byte
// 37.30us per byte, 26808.64 bytes per second
// tag := C6E906AABB009DA0

// 256 byte
// 32.84us per byte, 30448.18 bytes per second
// tag := 823C58872321394F

// 512 byte
// 30.61us per byte, 32665.50 bytes per second
// tag := 6EDAC26C4755A6B2

// 1024 byte
// 29.50us per byte, 33899.85 bytes per second
// tag := 3477763CA9DA92FE

void performance_test()
{
  const uint16_t TEST_REPETITION = 1000;
  uint32_t start_time;
  uint32_t elapsed_time;
  double us_per_byte;
  double byte_per_second;

  const uint8_t key1[8] = {0xf5, 0x26, 0x98, 0x26, 0xfc, 0x68, 0x12, 0x38};
  const uint8_t key2[8] = {0x9e, 0xb9, 0xd6, 0x40, 0xd0, 0x88, 0xda, 0x63};
  dove_mac_pp dmac(key1, key2);
  uint8_t tag[8];
  uint8_t theta[8];

  memset(tag, 0, sizeof(tag));
  dove_mac_pp::calc_theta(theta, message, sizeof(message));

  start_time = micros();
  for(uint16_t test_index = 0; test_index < TEST_REPETITION; ++test_index)
  {
    dmac.generate_mac(tag, theta, message, sizeof(message));
  }
  elapsed_time = micros() - start_time;
  us_per_byte = elapsed_time / ((double) TEST_REPETITION * sizeof(message));
  byte_per_second =
      (sizeof(message) * (double) TEST_REPETITION * 1000000.0) / elapsed_time;

  Serial.print(us_per_byte);
  Serial.print("us per byte, ");
  Serial.print(byte_per_second);
  Serial.println(" bytes per second");
  Serial.print("tag := ");
  Serial.print(((uint32_t*) tag)[1], HEX);
  Serial.println(((uint32_t*) tag)[0], HEX);
}

void setup()
{
  Serial.begin(9600);
  delay(1000);
  performance_test();
}

void loop()
{
  delay(10000);
}
