#include <Skinny.h>

struct TestVector
{
  byte key[8];
  byte tweak[8];
  byte plaintext[8];
  byte ciphertext[8];
};

static TestVector const tv_64_128_tw =
{
  {0x9e, 0xb9, 0x36, 0x40, 0xd0, 0x88, 0xda, 0x63},
  {0x76, 0xa3, 0x9d, 0x1c, 0x8b, 0xea, 0x71, 0xe1},
  {0xcf, 0x16, 0xcf, 0xe8, 0xfd, 0x0f, 0x98, 0xaa},
  {0x6c, 0xed, 0xa1, 0xf4, 0x3d, 0xe9, 0x2b, 0x9e}
};

byte buffer[8];
Skinny64_128_Tweaked *skinny64_128_tweaked;

void testCipher(Skinny64_128_Tweaked *cipher, const struct TestVector *tv)
{
  cipher->setKey(tv->key, skinny64_128_tweaked->keySize());
  cipher->setTweak(tv->tweak, skinny64_128_tweaked->keySize());
  cipher->encryptBlock(buffer, tv->plaintext);
  cipher->decryptBlock(buffer, buffer);

  if(memcmp(buffer, tv->plaintext, 8) == 0)
  {
    Serial.println("Skinny64-128-Tweaked Passed");
  }
  else
  {
    Serial.println("Skinny64-128-Tweaked Failed");
  }
}

void setup()
{
  Serial.begin(9600);
  delay(1000);
  Serial.println("setup");

  skinny64_128_tweaked = new Skinny64_128_Tweaked();
  testCipher(skinny64_128_tweaked, &tv_64_128_tw);
  delete skinny64_128_tweaked;
}

void loop()
{
  delay(10000);
}
