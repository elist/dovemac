#include "dove_mac.h"
#include "predefined_messages/message_64B.h"

// test results Atmega2560:
// 64 byte
// 47.49us per byte, 21056.79 bytes per second
// tag:= 679B57AB24B7F530

// 128 byte
// 38.49us per byte, 25982.69 bytes per second

// 256 byte
// 33.99us per byte, 29424.40 bytes per second

// 512 byte
// 31.73us per byte, 31511.41 bytes per second

// 1024 byte
// 30.61us per byte, 32670.01 bytes per second

// 2048 byte
// 30.05us per byte, 33281.87 bytes per second

// 4096 byte
// 29.77us per byte, 33596.47 bytes per second

// test results Atmega328p
// 64 byte
// 47.37us per byte, 21110.85 bytes per second
// tag:= 679B57AB24B7F530

// 128 byte
// 38.38us per byte, 12715.16 bytes per second
// tag := C6E906AABB009DA0

// 256 byte
// 33.88us per byte, 6846.79 bytes per second
// tag:= 823C58872321394F

// 512 byte
// 31.64us per byte, 3287.28 bytes per second
// tag := 6EDAC26C4755A6B2

// 1024 byte
// 30.51us per byte, 1310.90 bytes per second
// tag := 3477763CA9DA92FE

void performance_test()
{
  const uint16_t TEST_REPETITION = 1000;
  uint32_t start_time;
  uint32_t elapsed_time;
  double us_per_byte;
  double byte_per_second;

  const uint8_t key1[8] = {0xf5, 0x26, 0x98, 0x26, 0xfc, 0x68, 0x12, 0x38};
  const uint8_t key2[8] = {0x9e, 0xb9, 0xd6, 0x40, 0xd0, 0x88, 0xda, 0x63};
  dove_mac dmac(key1, key2);
  uint8_t tag[8];

  memset(tag, 0, sizeof(tag));

  start_time = micros();
  for(uint16_t test_index = 0; test_index < TEST_REPETITION; ++test_index)
  {
    dmac.generate_mac(tag, message, sizeof(message));
  }

  elapsed_time = micros() - start_time;
  us_per_byte = elapsed_time / ((double) TEST_REPETITION * sizeof(message));
  byte_per_second =
      (sizeof(message) * (double) TEST_REPETITION * 1000000.0) / elapsed_time;

  Serial.print(us_per_byte);
  Serial.print("us per byte, ");
  Serial.print(byte_per_second);
  Serial.println(" bytes per second");
  Serial.print("tag := ");
  Serial.print(((uint32_t*) tag)[1], HEX);
  Serial.println(((uint32_t*) tag)[0], HEX);
}

void setup()
{
  Serial.begin(9600);
  delay(1000);
  performance_test();
}

void loop()
{
  delay(10000);
}
