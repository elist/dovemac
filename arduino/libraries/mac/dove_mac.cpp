#include <string.h>
#include "utility/mac_util.h"

#include "dove_mac.h"

dove_mac::dove_mac(const uint8_t *key1, const uint8_t *key2) :
key1(key1),
key2(key2),
skinny64_128_tweaked(Skinny64_128_Tweaked()),
padded_message_block(NULL),
complete_message_block_number(0),
message_block_remainder(0),
complete_block_byte_number(0)
{}

dove_mac::~dove_mac()
{
  if(message_block_remainder)
  {
    delete[] padded_message_block;
  }
}

void dove_mac::generate_mac(uint8_t *tag, const uint8_t *message,
    size_t message_length)
{
  uint8_t u[CIPHER_BLOCK_SIZE];
  uint8_t s[CIPHER_BLOCK_SIZE];
  uint8_t x[CIPHER_BLOCK_SIZE];
  uint8_t y[CIPHER_BLOCK_SIZE];
  uint8_t theta[CIPHER_BLOCK_SIZE];
  const uint8_t *t;

  // init intermediate results
  memset(u, 0, CIPHER_BLOCK_SIZE);
  memset(s, 0, CIPHER_BLOCK_SIZE);
  memset(x, 0, CIPHER_BLOCK_SIZE);
  memset(y, 0, CIPHER_BLOCK_SIZE);
  memset(theta, 0, CIPHER_BLOCK_SIZE);

  this->prepare_message(message, message_length);
  skinny64_128_tweaked.setKey(this->key1, CIPHER_BLOCK_SIZE);

  // hashing
  for(uint16_t block_index = 0;
      block_index < this->complete_message_block_number; ++block_index)
  {
    // calculate start address of current message block
    t = message + (block_index << 4);
    process_message_block(t, s, u, x, y, theta);
  }
  // process padded message block
  if(message_block_remainder)
  {
    t = padded_message_block;
    process_message_block(t, s, u, x, y, theta);
  }
  mac_util::xor_64(x, theta);

  // finalizing
  skinny64_128_tweaked.setKey(this->key2, CIPHER_BLOCK_SIZE);
  skinny64_128_tweaked.setTweak(x, CIPHER_BLOCK_SIZE);
  skinny64_128_tweaked.encryptBlock(tag, y);
}

void dove_mac::process_message_block(const uint8_t *t, uint8_t *s, uint8_t *u,
    uint8_t *x, uint8_t *y, uint8_t* theta)
{
  /* calculate start address of the second message block half */
  const uint8_t* i = t + CIPHER_BLOCK_SIZE;
  mac_util::xor_64(theta, t);
  mac_util::xor_64(s, y, i);
  mac_util::xor_64(u, x, t);
  skinny64_128_tweaked.setTweak(u, CIPHER_BLOCK_SIZE);
  skinny64_128_tweaked.encryptBlock(x, s);
  mac_util::xor_64(y, x);
}

void dove_mac::set_key1(const uint8_t * key1)
{
  this->key1 = key1;
}

void dove_mac::set_key2(const uint8_t * key2)
{
  this->key2 = key2;
}

/* calculates the message block number and adds a padded message block at the
   end if necessary */
void dove_mac::prepare_message(const uint8_t *message, size_t message_size)
{
  // efficient modulo 16
  this->message_block_remainder = message_size & 0x0F;

  this->complete_block_byte_number = message_size - message_block_remainder;

  // efficient division by 16
  this->complete_message_block_number = complete_block_byte_number >> 4;

  /* if the message needs padding, that means the message size is not multiple
     of the message block size (16) */
  if(message_block_remainder)
  {
    this->append_bit_padding(message);
  }
}

// Creates a padded message block with the last bytes of the message
void dove_mac::append_bit_padding(const uint8_t *message)
{
  // allocate last message block
  this->padded_message_block = new uint8_t[MESSAGE_BLOCK_SIZE];

  // address of the first byte of the padded message block
  const uint8_t *first_byte_address = message + complete_block_byte_number;

  // byte index of padded message block
  uint8_t byte_index;

  // copy the bytes of the incomplete message block
  for(byte_index = 0; byte_index < message_block_remainder; ++byte_index)
  {
    padded_message_block[byte_index] = *(first_byte_address + byte_index);
  }

  // append bit padding
  padded_message_block[byte_index] = 0x80;
  for(byte_index += 1; byte_index < MESSAGE_BLOCK_SIZE; ++byte_index)
  {
    padded_message_block[byte_index] = 0;
  }
}