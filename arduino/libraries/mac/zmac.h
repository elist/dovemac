#ifndef ZMAC_H
#define ZMAC_H

#include "Skinny.h"

#define MESSAGE_BLOCK_SIZE 16
#define CIPHER_BLOCK_SIZE 8

class zmac
{
  public:
  zmac(const uint8_t *key1, const uint8_t *key2);
  ~zmac();
  void generate_mac(uint8_t *tag, const uint8_t *message,
      size_t message_length);
  void set_key1(const uint8_t * key1);
  void set_key2(const uint8_t * key2);

  private:
  void process_message_block(const uint8_t *xr, uint8_t *ll, uint8_t *lr,
      uint8_t *sl, uint8_t *sr, uint8_t *cl, uint8_t *cr, uint8_t *u,
      uint8_t *v);
  void prepare_message(const uint8_t *message, size_t message_size);
  void append_bit_padding(const uint8_t *message);

  private:
  const uint8_t *key1;
  const uint8_t *key2;
  Skinny64_128_Tweaked skinny64_128_tweaked;
  uint8_t *padded_message_block;
  uint16_t complete_message_block_number;
  uint8_t message_block_remainder;
  uint16_t complete_block_byte_number;
};

#endif