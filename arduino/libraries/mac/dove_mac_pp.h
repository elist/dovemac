#ifndef DOVE_MAC_PP_H
#define DOVE_MAC_PP_H

#include "Skinny.h"

#define CIPHER_BLOCK_SIZE 8

class dove_mac_pp
{
  public:
  static void calc_theta(uint8_t *theta, const uint8_t *message,
      size_t message_length);

  public:
  dove_mac_pp(const uint8_t *key1, const uint8_t *key2);
  void generate_mac(uint8_t *tag, const uint8_t *theta, const uint8_t *message,
      size_t message_length);
  void set_key1(const uint8_t * key1);
  void set_key2(const uint8_t * key2);

  private:
  void process_message_block(const uint8_t *t,uint8_t *s, uint8_t *u,
      uint8_t *x, uint8_t *y);

  private:
  const uint8_t *key1;
  const uint8_t *key2;
  Skinny64_128_Tweaked skinny64_128_tweaked;
};

#endif