#ifndef dove_mac_H
#define dove_mac_H

#include "Skinny.h"

#define MESSAGE_BLOCK_SIZE 16
#define CIPHER_BLOCK_SIZE 8

class dove_mac
{
  public:
  dove_mac(const uint8_t *key1, const uint8_t *key2);
  ~dove_mac();
  void generate_mac(uint8_t *tag, const uint8_t *message,
      size_t message_length);
  void set_key1(const uint8_t * key1);
  void set_key2(const uint8_t * key2);

  private:
  void process_message_block(const uint8_t *t,uint8_t *s, uint8_t *u,
      uint8_t *x, uint8_t *y, uint8_t* theta);
  void prepare_message(const uint8_t *message, size_t message_size);
  void append_bit_padding(const uint8_t *message);

  private:
  const uint8_t *key1;
  const uint8_t *key2;
  Skinny64_128_Tweaked skinny64_128_tweaked;
  uint8_t *padded_message_block;
  uint16_t complete_message_block_number;
  uint8_t message_block_remainder;
  uint16_t complete_block_byte_number;
};

#endif