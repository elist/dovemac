#include <string.h>
#include "mac_util.h"

#include "zmac.h"

zmac::zmac(const uint8_t *key1, const uint8_t *key2) :
key1(key1),
key2(key2),
skinny64_128_tweaked(Skinny64_128_Tweaked()),
padded_message_block(NULL),
complete_message_block_number(0),
message_block_remainder(0),
complete_block_byte_number(0)
{}

zmac::~zmac()
{
  if(message_block_remainder)
  {
    delete[] padded_message_block;
  }
}

void zmac::generate_mac(uint8_t *tag, const uint8_t *message,
    size_t message_length)
{
  uint8_t init_input[CIPHER_BLOCK_SIZE];
  uint8_t init_tweak[CIPHER_BLOCK_SIZE];
  uint8_t ll[CIPHER_BLOCK_SIZE];
  uint8_t lr[CIPHER_BLOCK_SIZE];
  uint8_t sl[CIPHER_BLOCK_SIZE];
  uint8_t sr[CIPHER_BLOCK_SIZE];
  uint8_t cl[CIPHER_BLOCK_SIZE];
  uint8_t cr[CIPHER_BLOCK_SIZE];
  uint8_t u[CIPHER_BLOCK_SIZE];
  uint8_t v[CIPHER_BLOCK_SIZE];
  const uint8_t *xr;

  // init intermediate results
  memset(sl, 0x00, CIPHER_BLOCK_SIZE);
  memset(sr, 0x00, CIPHER_BLOCK_SIZE);
  memset(cl, 0x00, CIPHER_BLOCK_SIZE);
  memset(cr, 0x00, CIPHER_BLOCK_SIZE);
  memset(u, 0x00, CIPHER_BLOCK_SIZE);
  memset(v, 0x00, CIPHER_BLOCK_SIZE);

  skinny64_128_tweaked.setKey(key1, skinny64_128_tweaked.keySize());

  // init ll, lr
  memset(init_input, 0x00, CIPHER_BLOCK_SIZE);
  memset(init_tweak, 0x00, CIPHER_BLOCK_SIZE);
  skinny64_128_tweaked.setTweak(init_tweak, CIPHER_BLOCK_SIZE);
  skinny64_128_tweaked.encryptBlock(ll, init_input);
  init_tweak[0] = 0x01;
  skinny64_128_tweaked.setTweak(init_tweak, CIPHER_BLOCK_SIZE);
  skinny64_128_tweaked.encryptBlock(lr, init_input);

  this->prepare_message(message, message_length);

  // hashing
  for(uint16_t block_index = 0;
      block_index < this->complete_message_block_number; ++block_index)
  {
    // calculate start address of current message block
    xr = message + (block_index << 4);
    process_message_block(xr, ll, lr, sl, sr, cl, cr, u, v);
  }
  // process padded message block
  if(message_block_remainder)
  {
    xr = padded_message_block;
    process_message_block(xr, ll, lr, sl, sr, cl, cr, u, v);
  }

  // finalizing
  skinny64_128_tweaked.setKey(key2, skinny64_128_tweaked.keySize());
  skinny64_128_tweaked.setTweak(u, CIPHER_BLOCK_SIZE);
  skinny64_128_tweaked.encryptBlock(tag, v);
}

void zmac::process_message_block(const uint8_t *xr,
    uint8_t *ll, uint8_t *lr, uint8_t *sl, uint8_t *sr, uint8_t *cl,
    uint8_t *cr, uint8_t *u, uint8_t *v)
{
  /* calculate start address of the second message block half */
  const uint8_t *xl = xr + CIPHER_BLOCK_SIZE;

  mac_util::xor_64(sl, xl, ll);
  mac_util::xor_64(sr, xr, lr);
  skinny64_128_tweaked.setTweak(sr, CIPHER_BLOCK_SIZE);
  skinny64_128_tweaked.encryptBlock(cl, sl);
  mac_util::xor_64(cr, cl, xr);

  mac_util::xor_64(u, cl);
  mac_util::mult2_64(u);
  mac_util::xor_64(v, cr);

  mac_util::mult2_64(ll);
  mac_util::mult2_64(lr);
}

void zmac::set_key1(const uint8_t *key1)
{
  this->key1 = key1;
}

void zmac::set_key2(const uint8_t *key2)
{
  this->key2 = key2;
}

/* calculates the message block number and adds a padded message block at the
   end if necessary */
void zmac::prepare_message(const uint8_t *message, size_t message_size)
{
  // efficient modulo 16
  this->message_block_remainder = message_size & 0x0F;

  this->complete_block_byte_number = message_size - message_block_remainder;

  // efficient division by 16
  this->complete_message_block_number = complete_block_byte_number >> 4;

  /* if the message needs padding, that means the message size is not multiple
     of the message block size (16) */
  if(message_block_remainder)
  {
    this->append_bit_padding(message);
  }
}

// Creates a padded message block with the last bytes of the message
void zmac::append_bit_padding(const uint8_t *message)
{
  // count message block with bit padding
  //++this->message_block_number;

  // allocate last message block
  this->padded_message_block = new uint8_t[MESSAGE_BLOCK_SIZE];

  // address of the first byte of the padded message block
  const uint8_t *first_byte_address = message + complete_block_byte_number;

  // byte index of padded message block
  uint8_t byte_index;

  // copy the bytes of the incomplete message block
  for(byte_index = 0; byte_index < message_block_remainder; ++byte_index)
  {
    padded_message_block[byte_index] = *(first_byte_address + byte_index);
  }

  // append bit padding
  padded_message_block[byte_index] = 0x80;
  for(byte_index += 1; byte_index < MESSAGE_BLOCK_SIZE; ++byte_index)
  {
    padded_message_block[byte_index] = 0;
  }
}