#ifndef MAC_UTIL_H
#define MAC_UTIL_H

#include <inttypes.h>

namespace mac_util
{
  void xor_64(uint8_t *x1, const uint8_t *x2);
  void xor_64(uint8_t *x, const uint8_t *x1, const uint8_t *x2);
  void mult2_64(uint8_t *a);
  uint8_t select(uint8_t a, uint8_t b, int8_t bit);
}

#endif