#include <string.h>
#include "utility/mac_util.h"

#include "dove_mac_pp.h"

void dove_mac_pp::calc_theta(uint8_t *theta, const uint8_t *message,
    size_t message_size)
{
  const uint8_t *t;
  uint16_t message_block_number = message_size >> 4;
  memset(theta, 0, CIPHER_BLOCK_SIZE);

  for(uint16_t block_index = 0;
      block_index < message_block_number; ++block_index)
  {
    // calculate start address of current message block
    t = message + (block_index << 4);
    mac_util::xor_64(theta, t);
  }
}

dove_mac_pp::dove_mac_pp(const uint8_t *key1, const uint8_t *key2) :
key1(key1),
key2(key2),
skinny64_128_tweaked(Skinny64_128_Tweaked())
{}

void dove_mac_pp::generate_mac(uint8_t *tag, const uint8_t *theta,
    const uint8_t *message, size_t message_size)
{
  uint8_t u[CIPHER_BLOCK_SIZE];
  uint8_t s[CIPHER_BLOCK_SIZE];
  uint8_t x[CIPHER_BLOCK_SIZE];
  uint8_t y[CIPHER_BLOCK_SIZE];
  const uint8_t *t;

  // init intermediate results
  memset(u, 0, CIPHER_BLOCK_SIZE);
  memset(s, 0, CIPHER_BLOCK_SIZE);
  memset(x, 0, CIPHER_BLOCK_SIZE);
  memset(y, 0, CIPHER_BLOCK_SIZE);

  this->skinny64_128_tweaked.setKey(this->key1, CIPHER_BLOCK_SIZE);

    // efficient division by 16
  uint16_t message_block_number = message_size >> 4;

  // hashing
  for(uint16_t block_index = 0;
      block_index < message_block_number; ++block_index)
  {
    // calculate start address of current message block
    t = message + (block_index << 4);
    this->process_message_block(t, s, u, x, y);
  }
  mac_util::xor_64(x, theta);

  // finalizing
  this->skinny64_128_tweaked.setKey(this->key2, CIPHER_BLOCK_SIZE);
  this->skinny64_128_tweaked.setTweak(x, CIPHER_BLOCK_SIZE);
  this->skinny64_128_tweaked.encryptBlock(tag, y);
}

void dove_mac_pp::process_message_block(const uint8_t *t, uint8_t *s,
    uint8_t *u, uint8_t *x, uint8_t *y)
{
  /* calculate start address of the second message block half */
  const uint8_t* i = t + CIPHER_BLOCK_SIZE;
  mac_util::xor_64(s, y, i);
  mac_util::xor_64(u, x, t);
  this->skinny64_128_tweaked.setTweak(u, CIPHER_BLOCK_SIZE);
  this->skinny64_128_tweaked.encryptBlock(x, s);
  mac_util::xor_64(y, x);
}

void dove_mac_pp::set_key1(const uint8_t * key1)
{
  this->key1 = key1;
}

void dove_mac_pp::set_key2(const uint8_t * key2)
{
  this->key2 = key2;
}
