#include "zmac.h"
#include "predefined_messages/message_64B.h"

void test_mac()
{
  const uint8_t key1[8] = {0xf5, 0x26, 0x98, 0x26, 0xfc, 0x68, 0x12, 0x38};
  const uint8_t key2[8] = {0x9e, 0xb9, 0xd6, 0x40, 0xd0, 0x88, 0xda, 0x63};
  zmac zm(key1, key2);
  uint8_t tag[8];

  memset(tag, 0, sizeof(tag));
  zm.generate_mac(tag, message, sizeof(message));
  Serial.print(((uint32_t*) tag)[1], HEX);
  Serial.println(((uint32_t*) tag)[0], HEX);
}

void setup()
{
  Serial.begin(9600);
  delay(1000);
  test_mac();
}

void loop()
{
  delay(10000);
}
